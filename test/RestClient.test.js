const restClientTest=require('../restclienttest');
jest.mock('../restclienttest');
describe('/GET Request Timeout API',()=>{
    it('should return sucess message if response is in 5s and request timeout is 30s',async()=>{
        restClientTest.get_DefaultRequestTimeout30s_res5_sucess.mockResolvedValue({
            message:"Sucessfully fetched data in 5 sec, default time is 30s"
        });
        const response=await restClientTest.get_DefaultRequestTimeout30s_res5_sucess();
        expect(response.message).toEqual('Successfully fetched data in 5 sec, Default time is 30s')
    })
});