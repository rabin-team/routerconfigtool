const { config } = require('dotenv');
const path = require('path');
rootdir = path.join(__dirname, '../');
config = require('config');
const moment = require('moment');
const express = require('express');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const app = express();

//Necessary for REST API
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(methodOverride('X-HTTP-Method-Override'));

//add CORS Support
app.use(function (req, res, next) {
    logger.info("Adding the CORS support inside the initilising function");
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    next();
});

logger = require('../util/Logutil');
const restClient = require('../util/RestClient');
app.use('/requestTimeoutAPI', function (req, res, next) {
    let { testcasename: testCaseName, setTimeOutInterval } = req.query;
    console.log('Received request from :%s', testCaseName);
    setTimeOut(() => {
        res.send("Response fetched Sucessfully")
    }, setTimeOutInterval);
});

app.listen(9111, () => {
    // console.log('Fast Dashboard Service');
});
let args_RequestTimeout10s = {
    data: {},
    requestConfig: {
        timeout: 10000
    }
}
let args_RequestTimeout35s = {
    data: {},
    requestConfig: {
        timeout: 35000
    }
}

/**
 * Test Cases for get method
 */
//Default Request Timeout 30 seconds
const get_DefaultRequestTimeout30s_ExpectResponseBack5s_ResponseBackSucess = () => {
    let startTime = moment().format('YYYY-MM-DD HH:mm:ss');
    let endTime;
    restClient.get('http://localhost:9111/requestTimeoutAPI?TimeOutInterval=5000&testcasename=get_DefaultRequestTimeout30s_ExpectResponseBack5s_ResponseBackSucess', null, (data, res) => {
        // console.log("get_DefaultRequestTimeout30s_ExpectResponseBack5s_ResponseBackSucess response back sucess:[statusCode:"+res.statusCode+",data:"+data+"]");
        endTime = moment().format('YYYY-MM-DD HH:mm:ss');
    }, (err) => {
        console.log('get_DefaultRequestTimeout30s_ExpectResponseBack5s_ResponseBackSucess time out error response:', err);
        endTime = moment().format('YYYY-MM-DD HH:mm:ss');
        console.log("get_DefaultRequestTimeout30s_ExpectResponseBack5s_ResponseBackSucess [" + startTime + "--->" + endTime + "]");
    });
}
module.exports = {
    get_DefaultRequestTimeout30s_res5_sucess: get_DefaultRequestTimeout30s_ExpectResponseBack5s_ResponseBackSucess
}