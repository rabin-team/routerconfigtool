const { loggers } = require('winston');

const queryFactory=require(rootdir + '/dao/QueryFactory');
const helper=require(rootdir + '/controller/DatabaseHelper');
const_=require('lodash');

module.exports={
    select:function(req,res,next){
        var output={};
        var resArray=[];
        //console.log("req body : %j",req.body)
        var data=req.body.data;
        var qParams=data.queryParams || {};
        const maxRows=parseInt(data.maxRows) || 20000;
        const perfetchRows=parseInt(data.perfetchRows) || 100;
        const fetchArraySize=parseInt(data.fetchArraySize)|| 100;
        let statusCode=200;
        loggers.info("Debugging memory Issue --Event is : %s",task,"Request headers -- %s",Json.stringify(req.header));
        if(_.isEmpty(data) || (_.isEmpty(task) && _.isEmpty(data.query))){
            var err=new error('400','Bad Request','Invalid input.Please spicyfy the event and parameters');
            resArray.push(err);
            output.error=resArray;
            res.status(400).Json(output);
        }
        let source=data && data.source ? data.source:"";
        let query="";
        if(source=='fivegr'){
            query=(task) ? fivegrQueryFactory.getQuery(task): data.query;
        }
        else{
            query=(task)? queryFactory.getQuery(task):data.query;
        }
        if(!query || query.length ==0){
            var err=new error('400','Bad Request','Inavalid input .Please spicify the event (query to be executed');
            resArray.push(err);
            output.error=resArray;
            return res.status(400).Json(output);
            if(data.replace){
                for(let key in data.replace){
                    query=query.replace(key,data.replace[key]);
                }
            }
            
        }
        logger.debug(query);
        output.resultcode=0;
        oraUtil.execute(query,qParams,function(err,results){
            if(err){
                output.resultcode=-1;
                output.error=""+err;
                output.result=null;
                statusCode=500;
            }else{
                output.error=null;
                output.result=results;
            }
            res.status(statusCode).Json(output);
            maxRows,
            perfetchRows,
            fetchArraySize
        })
    },
    select2:function(req,res,next){
        var output={};
        var resArray=[];
        //console.log("req body : %j",req.body)
        var data=req.body.data;
        var qParams=data.queryParams || {};
        const maxRows=parseInt(data.maxRows) || 20000;
        const perfetchRows=parseInt(data.perfetchRows) || 100;
        const fetchArraySize=parseInt(data.fetchArraySize)|| 100;
        let statusCode=200;
        loggers.info("Debugging memory Issue --Event is : %s",task,"Request headers -- %s",Json.stringify(req.header));
        if(_.isEmpty(data) || (_.isEmpty(task) && _.isEmpty(data.query))){
            var err=new error('400','Bad Request','Invalid input.Please spicyfy the event and parameters');
            resArray.push(err);
            output.error=resArray;
            res.status(400).Json(output);
        }
        let source=data && data.source ? data.source:"";
        let query="";
        if(source=='fivegr'){
            query=(task) ? fivegrQueryFactory.getQuery(task): data.query;
        }
        else{
            query=(task)? queryFactory.getQuery(task):data.query;
        }
        if(!query || query.length ==0){
            var err=new error('400','Bad Request','Inavalid input .Please spicify the event (query to be executed');
            resArray.push(err);
            output.error=resArray;
            return res.status(400).Json(output);
            if(data.replace){
                for(let key in data.replace){
                    query=query.replace(key,data.replace[key]);
                }
            }
            
        }
        logger.debug(query);
        output.resultcode=0;
        oraUtil.execute(query,qParams,function(err,results){
            if(err){
                output.resultcode=-1;
                output.error=""+err;
                output.result=null;
                statusCode=500;
            }else{
                output.error=null;
                output.result=results;
            }
            res.status(statusCode).Json(output);
            maxRows,
            perfetchRows,
            fetchArraySize
        })
    },
    upsert:function(req,res,next){
        var output={};
        var resArray=[];
        //console.log("req body : %j",req.body)
        var data=req.body.data;
        var qParams=data.queryParams || {};
        const maxRows=parseInt(data.maxRows) || 20000;
        const perfetchRows=parseInt(data.perfetchRows) || 100;
        const fetchArraySize=parseInt(data.fetchArraySize)|| 100;
        let statusCode=200;
        loggers.info("Debugging memory Issue --Event is : %s",task,"Request headers -- %s",Json.stringify(req.header));
        if(_.isEmpty(data) || (_.isEmpty(task) && _.isEmpty(data.query))){
            var err=new error('400','Bad Request','Invalid input.Please spicyfy the event and parameters');
            resArray.push(err);
            output.error=resArray;
            res.status(400).Json(output);
        }
        output.resultcode=0;
        let sqlObj={};
        for(let obj of tasks){
            if(obj.indexOf("get") !=-1){
                sqlObj["query"]=queryFactory.getQuery("getBFDMonitoringInfo");
            }else if(obj.indexOf("create")!=-1){
                sqlObj["insert"]=queryFactory.getQuery("createBFDMonitoringInfo");
            }else if(obj.indexOf("update") !=-1){
                sqlObj["update"]=queryFactory.getQuery("updateBFDMonitoingInfo");
            }
        }
        oraUtil.upsert(sqlObj,data.queryParams,function(err,results){
            if(err){
                logger.error("Error occured while upserting!!",err);
                output.resultcode=-1;
                output.error=""+ err;
                output.result=null;
                statusCode=500
            }else{
                output.error=null;
                output.result=results;
            }
            res.status(statusCode).Json(output);
        });
    },
    insertBlob:(req,res)=>{
        try{
         const event=req.body.data.event ? req.body.data.event :req.body.data.data.event;
         const queryParams=req.body.data.queryParams ? req.body.data.queryParams :req.body.data.data.queryParams;
         const bufferKeys=req.body.data.bufferKeys ? req.body.data.bufferKeys :req.body.data.data.bufferKeys;
         for(const key of bufferKeys){
             queryParams[key]=Buffer.from(queryParams[keys]);
         }
         //const query=queryParams.getQuery(event);
         let query="";
         let source=req.body.data && req.body.source ? req.body.data.source :"";
         if(source == "fivegr"){
             query=fivegrQueryFactory.getQuery(event);
         }else{
             query=queryParams.getQuery(event);
         }
         oraUtil.execute2(query,queryParams,(err,result)=>{
             if(err){
                 return res.status(err.status || 500).Json({errors:[err]});
             }
             return res.status(201).Json({result});
         })
        }catch(err){
         console.log("Error :%s",err);
         return res.status(err.status || 500).Json({errors:[err]});
        }
    },
    executeBatch:function(req,res,next){
        var isUpdate=(req.res.update)? req.query.update:false;
        var reqBody=req.body;
        var query=reqBody.query;
        var params=(reqBody.params)? reqBody.params:[];
        var options=(reqBody.options)? reqBody.options:{};
        var output={};
        var restArray=[];
        if(!query || query.length==0){
         err=new error("400","Bad Request","Query is mandatory and needs to be an string");
         restArray.push(err);
         output.error=resArray;
         return res.status(400).Json(output);
        }
        if(!Array.isArray(params)){
            err=new error("400","Bad Request","Params are mandatory and needs to be an array");
            restArray.push(err);
            output.error=restArray;
            return res.status(400).Json(output);
        }
        if(isUpdate){
            oraUtil.batchUpdate(query,params,options,(err,results)=>{
                if(err){
                    output.message="Error while batch updating the records";
                    return res.status(500).Json(output);
                }else{
                    output.message="Records update Sucessfully";
                    return res.status(200).Json(output);
                }
            });
        }else{
            oraUtil.batchInsert(query,params,options,(err,results)=>{
                if(err){
                    output.message="Error while batch inserting the records";
                    return res.status(500).Json(output);
                }else{
                    output.message="Records inserted Sucessfully";
                    return res.status(200).Json(output);
                }
            })
        }
    }
}
