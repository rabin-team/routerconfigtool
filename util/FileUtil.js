var fs=require('fs');
const { endianness } = require('os');

var fileUtil=function(){
    logger.info('Initializing the file util');
}
fileUtil.prototype.readFile=function(filePath,encoding,callback){
    fs.readFile(filePath,encoding,callback);
};

fileUtil.prototype.write=function(fileObj,encoding,callback){
    let output={};
    output.result_code=0;
    output.error={};
    encoding=encoding || 'uft-8';
    let filePath=fileObj.path;
    let fileName=fileObj.name;
    let fullFilePath=filePath+'/'+fileName;
    let data=fileObj.content;

    //async write
    if(typeof callback ==="function"){
        fs.writeFile(fullFilePath,data,encoding,(err)=>{
            console.log("Error writing the file ... :%s",err);
            if(err){
                output.result_code=err.errno;
                output.error=err;
            }
            delete fileObj.content;
            output.file=fileObj;
            return callback(output);
        });
    }else{
        try{
           let res=fs.writeFileSync(file,data,encoding);
           if(res === undefined){
            return output;
           }else{
            output.error= -1;
            return output;
           } 
        }catch(e){
            console.log("Error creating file :%j",e);
            output.error=e;
            output.result_code=e.errno;
            return output;
        }
    }
}
module.exports=new fileUtil();