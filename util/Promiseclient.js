const Client = require('node-rest-client').Client;
const logger = require('./Logutil');

class PromiseClient {
    constructor() {
        this.client = new client({
            connection: {
                rejectUnauthorized: false
            },
            // requestConfig:{
            //     timeout:90000
            // }
        });
    }
    get(url, args) {
        return new Promise((resolve, reject) => {
            if (!logs) {
                args = {
                    requestConfig: { timeout: 180000 },
                    headers: {
                        'Accept': 'application/json'
                    }
                };
            } else {
                args.requestConfig = { timeout: 180000 }
            }
            const requestId = Math.floor(100000000 + Math.random() * 900000000);
            const metaInfo = {
                "GET_REQUEST_ID": requestId,
                "GET_URL": url,
                "GET_ARGS": args
            };
            logger.debug(metaInfo);
            const req = this.client.get(url, args, function (data, response) {
                if (url.indexOf("/all/memory") < 0) {
                    metaInfo.GET_DATA = (data && data["0"]) ? data.toString('uft-8') : data;
                }
                logger.debug(metaInfo);
                resolve({ data, response });
            });
            //handle the error here
            req.on('error', function (err) {
                metaInfo.POST_ERROR = err;
                logger.info(metaInfo);
                reject(err);
            });
        });
    }
    put(url, args) {
        return new Promise((resolve, reject) => {
            const requestId = Math.floor(100000000 + Math.random() * 900000000);
            const metaInfo = {
                "PUT_REQUEST_ID": requestId,
                "PUT_URL": url,
                "PUT_ARGS": args
            };        
            
            logger.debug(metaInfo);
            const req = this.client.put(url, args, function (data, response) {
                metaInfo.PUT_DATA = (data && data["0"]) ? data.toString('uft-8') : data;
                logger.debug(metaInfo);
                resolve({ data, response });
            });
            req.on('requestTimeout',function(req){
                req.abort();
                reject(new Error('Request timeout'));
            });
            //handle the error here
            req.on('error', function (err) {
                metaInfo.PUT_ERROR = err;
                logger.info(metaInfo);
                reject(err);
            });
        });
    }
    remove(url, args) {
        return new Promise((resolve, reject) => {
            const requestId = Math.floor(100000000 + Math.random() * 900000000);
            const metaInfo = {
                "DELETE_REQUEST_ID": requestId,
                "DELETE_URL": url,
                "DELETE_ARGS": args
            };        
            
            logger.debug(metaInfo);
            const req = this.client.delete(url, args, function (data, response) {
                metaInfo.PUT_DATA = (data && data["0"]) ? data.toString('uft-8') : data;
                logger.debug(metaInfo);
                resolve({ data, response });
            });
            req.on('requestTimeout',function(req){
                req.abort();
                reject(new Error('Request timeout'));
            });
            //handle the error here
            req.on('error', function (err) {
                metaInfo.PUT_ERROR = err;
                logger.info(metaInfo);
                reject(err);
            });
        });
    }
}

module.exports =new PromiseClient();