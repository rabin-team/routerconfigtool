var geolib = require('geolib')

var CommonUtil = function () {
    logger.debug("Fetching information on the test types")
}
CommonUtil.prototype.calculateDistance = function (source, destination, unit) {
    unit = (unit) ? unit.toUpperCase() : "MILES";
    var distance = geolib.getDistance(source, destination).toFixed(2);
    var distanceText = "";
    switch (unit) {
        case "MILES":
            distanceText = (distance * 0.00621371).toFixed(2) + "Miles";
            break;
        case "KMS":
            distanceText = (distance * 0.001).toFixed(2) + "Kms";
            break;
        case "FEET":
            distanceText = (distance * 3.28084).toFixed(2) + "Feet";
            break;
        default:
            distanceText = (distance * 0.00621371).toFixed(2) + "Miles";
    }
    var o = { distanceText: distanceText, distance: distance };
    return o;
};

module.exports=new CommonUtil();