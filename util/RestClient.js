var Client=require('node-rest-client').Client;
var constrants=require('constants');
const { futimesSync } = require('fs');
const { resolve } = require('path');
var util=require('util');
const HttpError=require('../model/HttpError');
const config=require('config');
const DefaultTimeOut=30000; // 30 seconds
const RequestDefaultTimeOut=config?.timeout?.requestTimeout || DefaultTimeOut;
const ResponseDefaultTimeOut=config?.timeout?.responseTimeout || DefaultTimeOut;

let self=null;
var RestClient=function(){
    this.client=new client({
        requestConfig:{
            timeout:RequestDefaultTimeOut //request timeout
        },
        responseConfig:{
            timeout:ResponseDefaultTimeOut //request timeout
        }
    });
    self=this;
};

RestClient.prototype.get=function(url,args,callback,errorHandler){
    var requestId=Math.floor(100000000+Math.random()* 900000000);
    var metaInfo={
            "GET_REQUEST_ID": requestId,
            "GET_URL": url,
            "GET_ARGS": args
        };
        logger.debug(metaInfo);
        const req = this.client.get(url, args, function (data, response) {
            if (url.indexOf("/all/memory") < 0) {
                metaInfo.GET_DATA = (data && data["0"]) ? data.toString('uft-8') : data;
            }
            logger.debug(metaInfo);
            callback({ data, response });
        });
        //On Request timeout sending null and response with status code 504
        req.on('requestTimeout',function(req){
            let response={
                statusCode:504
            };
          return callback(null,response);
        }) ;
         //Handle the error here
         req.on('error',function(err){
            metaInfo.GET_ERROR=err;
            logger.info(metaInfo);
            if(errorHandler){
                errorHandler(err);
            }
    });
};
RestClient.prototype.post=function(url,args,callback,errorHandler){
    var requestId=Math.floor(100000000+Math.random()* 900000000);
    var metaInfo={
            "POST_REQUEST_ID": requestId,
            "POST_URL": url,
  //          "POST_ARGS": args
        };
        logger.debug(metaInfo);
        const req = this.client.get(url, args, function (data, response) {
            if (url.indexOf("/all/memory") < 0) {
                metaInfo.POST_DATA = (data && data["0"]) ? data.toString('uft-8') : data;
                logger.debug(metaInfo);
                callback({ data, response });
            }
           
        });
        
         //Handle the error here
         req.on('error',function(err){
            metaInfo.POST_ERROR=err;
            logger.info(metaInfo);
            if(errorHandler){
                errorHandler(err);
            }
    });
};
RestClient.prototype.put=function(url,args,callback,errorHandler){
    var requestId=Math.floor(100000000+Math.random()* 900000000);
    var metaInfo={
            "PUT_REQUEST_ID": requestId,
            "PUT_URL": url,
            "PUT_ARGS": args
        };
        logger.debug(metaInfo);
        const req = this.client.put(url, args, function (data, response) {
            if (url.indexOf("/all/memory") < 0) {
                metaInfo.PUT_DATA = (data && data["0"]) ? data.toString('uft-8') : data;
                logger.debug(metaInfo);
                callback({ data, response });
            }
           
        });
        
         //Handle the error here
         req.on('error',function(err){
            metaInfo.PUT_ERROR=err;
            logger.info(metaInfo);
            if(errorHandler){
                errorHandler(err);
            }
    });
};
RestClient.prototype.remove=function(url,args,callback,errorHandler){
    var requestId=Math.floor(100000000+Math.random()* 900000000);
    var metaInfo={
            "DELETE_REQUEST_ID": requestId,
            "DELETE_URL": url,
            "DELETE_ARGS": args
        };
        logger.debug(metaInfo);
        const req = this.client.delete(url, args, function (data, response) {
            if (url.indexOf("/all/memory") < 0) {
                metaInfo.DELETE_DATA = (data && data["0"]) ? data.toString('uft-8') : data;
                logger.debug(metaInfo);
                callback({ data, response });
            }
           
        });
        
         //Handle the error here
         req.on('error',function(err){
            metaInfo.DELETE_ERROR=err;
            logger.info(metaInfo);
            if(errorHandler){
                errorHandler(err);
            }
    });
};
/**
 * @typedef {{
 * 'headers':{
 * "Accept":string,
 *  "Content-Type":string
 *   'Authorization'?:string
 * },
 * 'data':{}
 * }}RequestArgs
 */

RestClient.prototype.promises={
    /**
     * 
    * @param {string} url 
    * @param {RequestArgs} args meta data of the request -accept and Content-type headers are defaulted to json
    * @returns {Promise<Response} response of request
    * @throws {HttpError} throws if the status code of response is between 400-599
    * @throws {HttpError}  throws if there is no response -ex :timeout or socket error
     */
   get:function(url,args){
    args=getDefualtRequestArgs(args);
    return new Promise((resolve,reject)=>{
        self.get(url,args || {},(data,res)=>{
            if(res.statusCode>=400 && res.statusCode<=599){
                return reject(new HttpError(data,res));
            }
            resolve({data,response:res});
        },err=>{
            reject(new HttpError(null,null,err));
        });
    });
   },
   /**
    * 
    * @param {string} url 
    * @param {RequestArgs} args meta data of the request -accept and Content-type headers are defaulted to json
    * @returns {Promise<Response} response of request
    * @throws {HttpError} throws if the status code of response is between 400-599
    */
   post:function(url,args){
    args=getDefualtRequestArgs(args);
    return new Promise((resolve,reject)=>{
        self.post(url,args || {},(data,res)=>{
            if(res.statusCode>=400 && res.statusCode<=599){
                return reject(new HttpError(data,res));
            }
            resolve({data,response:res});
        },err=>{
            reject(new HttpError(null,null,err));
        });
    });
   },
}
/**
 * sets headers of args to there defaults
 * @param {RequestArgs} args 
 * @returns 
 */
function getDefualtRequestArgs(args){
    /**
     * Added for default timeout JIRA number
     */
    args=args || {}
    let requestTimeout=args?.requestTimeout?.timeout || RequestDefaultTimeOut;
    let responsetimeout=args?.requestTimeout?.timeout || RequestDefaultTimeOut;
    args={
        ...args,
        requestConfig:{
            timeout:requestTimeout
        },
        responseConfig:{
            timeout:responsetimeout
        }
    }
    return {
        ...args,
        headers:{
            ...Client(args && args.headers),
            'Accept':(args && args.headers && args.headers['Accept']) || 'application/json',
            'Content-Type':(args && args.headers && args.headers['Content-Type']) || 'application/json'
        }
    }
}
module.exports=new RestClient();