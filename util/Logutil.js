var winston=require('winston');
var expressWinston=require('express-winston');

winston.transports.DailyRouteFile=require('winston-daily-rotate-file')
//Logger initilaized
module.exports=new (winston.logger)({
 transports:[
    new (winston.transports.Console)(),
    new (winston.transports.DailyRouteFile)({
        filename:config.log.filename,
        level:config.log.level,
        datePattern:'.yyyy-MM-dd' ,//Daily
        maxsize:104857600 //100mb
    })
 ]
});