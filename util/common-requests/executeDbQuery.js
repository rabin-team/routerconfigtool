const config=require('config');

const client=require('../Promiseclient');
const logger=require('../Logutil');
const IopError=require('../../model/HttpError');

async function executeDbQuery(eventName,bindArgs){
    const url=config.dbService.url+'get';
    const args={
        headers:{
            'Content-Type':'application/json',
            'Accept':'application/json'
        },
        data:{
            data:{
                queryParams:bindArgs,
                event:eventName
            }
        }
    };
    try{
     const {data,response}=await client.post(url,args);
     if(data && response && response.statusCose === 200){
        return data;
     }else{
        const error=new IopError(500,'Server Error','There was an error executing the query for the event'+eventName);
        console.info(error);
        throw error;
     }
    }catch(err){
        throw err;
    }
}
module.exports=executeDbQuery;