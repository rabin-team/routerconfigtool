const fs=require('fs');
const path=require('path')

const RouterParamHelper=require('../model/RouterConfigModel')

function generateUnformattedNokiaConfig(router_info){
    const file =path.resolve(__dirname,'../template/Nokia7705/Nokia7705UnformattedTemplate');
    let template =fs.readFileSync(file).toString();
}

function replaceVariablesWithRouterInfo(template,reqBody){
    const routerParams= RouterParamHelper.generateNokiaParams(reqBody);
    for(const entry of Object.keys(routerParams)){
        template=template.replace(entry,routerParams[entry]);
    }
    return template;
}