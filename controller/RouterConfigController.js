// ItentialController.js
let error = require('../model/globalError');
let restClient=require(rootdir + '/util/RestClient')
const client=require('../util/PromiseClient');
const fetch = require("node-fetch")
const moment = require('moment');
const DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss'
const JSZip = require('jsZip');
const fs=require('fs');
const { exec } = require('child_process');
const { response } = require('child_process');
const unzipper=require('unzipper');
const { time } = require('console');
const { CONNREFUSED } = require('dns');
const { config } = require('process');
const executeDBQuery=require('../util/common-requests/executeDbQuery');
const { errorMonitor } = require('events');
const oneEMSService=require('../services/oneEMSService')
let routerHelper =require(rootdir + '/controller/RouterConfigHelper')


module.exports={
    generateRouterConfig:async function(req,res,next){
        let configObj=req.body;
        let resArray=[];
        let output={};
        if(configObj.config_params && configObj.config_params.csr_vendor && configObj.config_params.csr_vendor.toUpperCase()=="ALU(NOKIA"){
            if(!configObj.config_params){
                logger.info("No config params in the request");
                let err=new error(
                    "400",
                    "Bad Request",
                    "Config params cannot be null/undefined"
                );
                resArray.push(err);
                output.errors=resArray;
                return res.status(400).json(output);
            }else if(
                !configObj.config_params.csr_vendor ||
                (configObj.config_params.csr_vendor.toUpperCase()!=="ALU(NOKIA" && 
                configObj.config_params.csr_vendor.toUpperCase()!=="CISCO")
            ){
                logger.info("Not a valid vendor type selected");
                let err=new errorMonitor("400","Bad Request","Not a valid  vendor type");
                resArray.push(err);
                output.errors=resArray;
                return res.status(400).json(output);
            }else if(configObj.config_params.created_by){
                logger.info("No user id selected in the request");
                let err=new errorMonitor("400","Bad Request","Not a valid  vendor type");
                resArray.push(err);
                output.errors=resArray;
                return res.status(400).json(output);
            }
            let reqData={};
            reqData.wf_inst_id=configObj.wf_inst_id;
            reqData.task_inst_id=configObj.task_inst_id;
            reqData.user_id=configObj.project_id;
            reqData.action=config.actions.routerConfig.generate;
            if(configObj.wf_inst_id && configObj.wf_inst_id !=null && configObj.wf_inst_id.length !==0){
                configObj.user_id=configObj.config_params.created_by;
                routerHelper.addRouterValues(configObj);
            }
            let msc_router=configObj.config_params.msc_router;
            if(msc_router && msc_router.new_even_port){
                msc_router.new_even_port=msc_router.new_even_port;
            }
            if(msc_router && msc_router.new_odd_port){
                msc_router.new_odd_port=msc_router.new_odd_port;
            }
            configObj.config_params.switch_name=(configObj.config_params.switch)? configObj.config_params.switch:configObj.config_params.switch_name;
            getNokia(configObj,function(err,result){
                if(err){
                    res.status(err.status).json(result);
                }else{
                    trackTaskActions(reqData);
                    res.status(200).json(result);
                }
            });
        }else if(configObj.config_params && configObj.config_params.csr_vendor && configObj.config_params.csr_vendor.toUpperCase()=="CISCO"){
            try{
                if(configObj.config_params && configObj.config_params.csr_vendor && configObj.config_params.csr_vendor.toUpperCase()=="SPOKE"){
                    let validatorArray=["ASR900"]
                    configObj.config_params.asr_sproks.hub_equipment_type=(configObj.config_params.asr_sproks.hub_equipment_type &&  configObj.config_params.asr_sproks.hub_equipment_type.toUpperCase()==='NCS5501')?'NCS5500':configObj.config_params.asr_sproks.hub_equipment_type;
                    if(validatorArray.indexOf(configObj.config_params?.asr_sproks.hub_equipment_type?.toUpperCase())<0){
                        logger.info("Invalid hub equipment type");
                        let err=new error("400","Bad Request","Not a valid hub equipment type for spoke");
                        resArray.push(err);
                        output.errors=resArray;
                        return res.status(400).json(output);
                    }
                }
                let errorOut=this.generateRouterConfigvalidation(configObj.config_params)
                if(errorOut.error.length>0){
                  console.log('errors output:',errorOut.errors)
                  output.errors=errorOut.errors;
                  return res.status(400).json(output);
                }
                const configResult=await oneEMSService.getOneEmsConfigs(configObj.config_params);
                return res.status(200).json({config_result:configResult});
            }catch(err){
                logger.info("Configuration error occured",err);
                let output={};
                let resArray=[];
                let e=new error("400","Bad Request",err.messgae || err.detail || 'Unknown error occured');
                resArray.push(e);
                output.errors=resArray;
                return res.status(400).json(output);
            }
        }
    }
}