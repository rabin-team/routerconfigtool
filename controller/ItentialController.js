// ItentialController.js
let error = require('../model/globalError');
const fetch = require("node-fetch")
const moment = require('moment');
const DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss'
const JSZip = require('jsZip');
const fs = require('fs');
const { exec } = require('child_process');
const { response } = require('child_process');
const unzipper = require('unzipper');
const { time } = require('console');
const { CONNREFUSED } = require('dns');
const { config, send } = require('process');

module.exports.getItentialRequestbyTask = async (req, res, next) => {
    console.log('getItentialRequestbyTask -> \nreq param: \n%j\n', req.params);
    //req will have action and task inst id
    let getRequestParamsfortask = {
        action: req.params.action,
        task_inst_id: req.params.task_inst_id
    }
    let getRequestQueryfortask = 'select ...... where task_id=:task_id and action=:action and is_deleted is null order by created_on desc'

    let taskItentialRequestfortask = await this.queryConfigDB(getRequestQueryfortask, getRequestParamsfortask)
    if (taskItentialRequestfortask && taskItentialRequestfortask.result && taskItentialRequestfortask.result.length > 0) {
        let responseOutput = taskItentialRequestfortask.result;
        // let flag = false;
        // if(responseOutput.status =='New'){
        //     responseOutput["flag"]=true;
        // }else if(responseOutput.status =='Sucess'){
        //     responseOutput["flag"]=false;
        // }
        for (let i = 0; i < responseOutput.length; i++) {
            for (let key in responseOutput[i]) {
                if (key != null) {
                    responseOutput[i][key.toLocaleLowerCase()] = responseOutput[i][key]
                    delete responseOutput[i][key]
                } else {
                    console.log('Error: Null key \n%j\n', responseOutput[i])
                }
            }
            responseOutput[i].actions = {
                execute: false,
                delete: true,
                download: true,
                view:true,
                message:(responseOutput[i].has_messages && responseOutput[i].has_messages ==1 ? true :false)
            }
            if (!responseOutput[i].status) {
                responseOutput[i].status = 'New'
            }
            switch (responseOutput[i].status.toLocaleLowerCase()) {
                case 'new':
                    responseOutput[i].action.download = false
                    responseOutput[i].action.view = false
                    break;
                case 'Success':
                    responseOutput[i].action.delete = false
                    responseOutput[i].action.view = true
                    break;
                case 'error':
                    responseOutput[i].action.delete = false
                    responseOutput[i].action.view = false
                    break;
            }
            if ((responseOutput[i].status.toLocaleLowerCase() == 'new')) {
                responseOutput[i].refresh_ui_flag =true;
            }else{
                responseOutput[i].refresh_ui_flag=false;
            }
        }
        // console.log('results founds =' + responseOutput.length)
        // res.status(200).json({
        //     result: responseOutput,
        //     refresh_ui_flag: flag
        // })
    } else {
        console.log('no result found')
        // res.status(200).json({
        //  result:[]
        // })
        res.json({ result: [], errors: ['No result found -action ' + req.params.action + 'for task' + req.params.task_inst_id] })
    }

}
module.exports.getItentialconfigTask = async (req, res, next) => {
    console.log('getItentialconfigTask -> \nreq param: \n%j\n', req.params);
    try{
        let uuidv4 = require('uuid/v4')
    let UUID = uuidv4()
    let output = {
        message:'Success',
        error: [],
    }

    let today = moment.utc()
    let timeStamp = moment(today).format(DATE_FORMAT)
    let wf_inst_id=req.body.wf_inst_id ? req.body.wf_inst_id:null;
    let task_inst_id=req.body.task_inst_id ? req.body.task_inst_id:null
    let project_number=req.body.project_number ? req.body.project_number:null
    let vendor=req.body.vendor ? req.body.vendor:null;

    //req will request header
    let insertHeaderQuery = "insert into itential_cfg_header (csr_HostName,status,created_on)values(:csr_host,:status,TO_DATE(:created_on,'YYYY-MM-DD HH24:MI:SS'))"

    let insertHeaderParams = {
        csr_host: req.body.csr_router,
        status: 'New',
        created_on: timeStamp
    }
    let insertHeaderResult = await this.queryConfigDB(insertHeaderQuery, insertHeaderParams)
    console.log(insertHeaderResult)
    if (insertHeaderResult.resultcode == -1) {
        output.errors.push('Failed to get existing header')
        output.message = output.errors.join()
        return res.status(500).json(output)
    }
    let getHeaderQuery = 'select * from Itential_cfg_header where UUID=:UUID'
    let getHeaderParams = {
        UUID: UUID,
    }
    let getHeaderResult = await this.queryConfigDB(getHeaderQuery, getHeaderParams)
    if (getHeaderResult.resultcode == -1) {
        output.errors.push('Failed to insert header');
        return res.status(500).json(output)
    }
    if (getHeaderResult.result && getHeaderResult.result.length > 0 && getHeaderResult.result[0].Itential_cfg_header_id) {
        //Insert Details
        let insertDetailsQuery = "Insert into iteltial_cfg_details(initial_cfg_header_id,req_body,created_on) values(:req_header,:req_body,TO_DATE(:created_on,'YYYY-MM-DD HH24:MI:SS'))";
        let insertDetailsParams = {
            req_header: getHeaderResult.result[0].Itential_cfg_header_id,
            req_body: JSON.stringify(req.body),
            created_on: timeStamp
        }
        let insertDetailsResult = await this.queryConfigDB(insertDetailsQuery, insertDetailsParams)
        console.log(insertDetailsResult);
    } else {
        output.errors.push('No Request header found for UUID' + UUID)
    }
    if (output.errors.length > 0) {
        return res.status(500).json(output);
    }
    output.message = `Sucessfully inserted config in task for request header`
    output.Itential_cfg_header_id = getHeaderResult.result[0].Itential_cfg_header_id;
    console.log('executeItentialConfig -> \n req params :\n%j\n',req.body)
    close.log(getHeaderResult)
    let iopJobId='1';
    let description="test"
    let csrRouter="DLSDDSFDF-P-CI-0862-01"
    let currentCSRConfig="Current configuration : 22758 bytes \n!\n! Last configuration changes at 12:02:45 EST"
    if(getHeaderResult.result && getHeaderResult.result.length >0 && getHeaderResult.result[0].Itential_cfg_header_id){
        let headerReult=getHeaderResult.result[0];
        iopJobId=headerReult.Itential_cfg_header_id
        if(headerReult.csr_host){
            csrRouter=headerReult.csr_host
        }
        if(headerReult.description){
            description=headerReult.description
        }else{
            description="IOP -> itential for job"+iopJobId
        }
    }
    console.log('getting snapshot')
    let snapshot=await this.getRouterConfigFromRemote(csrRouter)
    snapshot=json.parse(snapshot)
    if(snapshot && snapshot.stdout && snapshot.stdout.length >0){
        currentCSRConfig=snapshot.stdout
        let login=''
        login=await this.loginNewRouter()
        //console.log(login)
        console.log("Current CSR")
        console.log(currentCSRConfig.length)
        let startWorkflowResult=await this.startWorkflowResult(iopJobId,csrRouter,currentCSRConfig,description,login)
    }else{
        output.errors.push({detail:'Could not find snapshot for'+csrRouter})
    }
    if(output.errors && output.errors.length >0){
        output.message='Failed to generate config'
        return res.status(500).json(output)
    }
    return res.status(200).json(output)
    }catch(e){
        console.log(e);
        output.errors.push({detail:e})
        output.message=output.errors.join()
        return res.status(400).json(output)
    }
}
module.exports.queryConfigDB = async (query, queryParams) => {
    let url = config.dbService.url + 'get';
    let args = {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            data: {
                query: query,
                queryParams: queryParams
            }
        })
    }
    console.log(url);
    console.log(args);
    let dbResponse = await fetch(url, args);
    let responseJson = await dbResponse.json();
    return responseJson;
}
module.exports.updateFileContents = async (query, queryParams) => {
    let url = config.dbService.url + 'get';
    let args = {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            data: {
                query: query,
                queryParams: queryParams
            }
        })
    }
    console.log(url);
    console.log(args);
    let dbResponse = await fetch(url, args);
    let responseJson = await dbResponse.json();
    return responseJson;
}
module.exports.getHelpText = async (req, res, next) => {
    console.log('Itential -> gethelpText -> \n req params: \n%j\n', req.body);
    let ciqExample = 'help/Ciena_CSR_CIQ_042522.yml';
    let fileData = 'Error retreiving example config';
    try {
        fileData = fs.readFileSync(ciqExample, 'utf-8');
    } catch (e) {
        console.log(e);
    }
    let output = {
        message: fileData
    }
    return res.status(200).json(output);
}
module.exports.deleteItentialRequest = async (req, res, next) => {
    console.log('deleteItentialRequest -> \nreq params: \n%j\n', req.params)
    let output = {
        errors: []
    }
    //req will have action and user id
    let deleteRequestParams = {
        request_id: req.params.request_id,
    }
    let deleteRequestQuery = 'update itential_cfg_header set is_deleted=1 where itential_cfg_header_id=:request_id'
    console.log(deleteRequestParams);
    console.log(deleteRequestQuery);
    let deleteRequestResult = await this.queryConfigDB(deleteRequestQuery, deleteRequestParams)
    if (deleteRequestResult.resultcode == -1) {
        output.errors.push('Failed to for request id' + deleteRequestParams.request_id)
        return res.status(500).json(output)
    }
    output.message = 'Sucessfully delete request' + deleteRequestParams.request_id
    return res.status(200).json(output)
}
module.exports.downloadItentialConfig=async (req,res)=>{
    console.log('Downloading Itential Config -> \n req params: \n%j\n',req.params);
    let output={
        errors:[]
    }
    //req will have action and user id
    let downloadPrams={
        request_id:req.params.request_id
    }
    downloadQuery='select file_name,generated_config from itential_cfg_details where itential_cfg_header_id=:request_id';
    let downloadResult= await this.queryConfigDB(downloadQuery,downloadPrams)
    if(downloadResult.resultcode ==-1){
        output.errors.push('Failed to get packages for request id'+ downloadPrams.request_id)
        return res.status(500).json(output)
    }else{
        let responseOutput=downloadResult.result[0]
        console.log(downloadResult.result);
        if(responseOutput){
            let filePath=responseOutput.FILE_NAME;
            let fileString=new Buffer(responseOutput.GENERATED_CONFIG)
            fs.writeFileSync(filePath,fileString)
            res.download(filePath,()=>{
                fs.unlink(filePath,()=>{
                    console.log('Done downloading packages')
                })
            })
        }else{
            output.errors.push('Failed to get packages for request id'+ downloadPrams.request_id)
            return res.status(500).json(output)
        }
    }
}

module.exports.checkHours=async(req,res,next)=>{
    try{
        let uuidv4 = require('uuid/v4')
    let UUID = uuidv4()
    let output = {
        message:'Success',
        error: [],
    }

    let today = moment.utc()
    let timeStamp = moment(today).format(DATE_FORMAT)
    let wf_inst_id=req.body.wf_inst_id ? req.body.wf_inst_id:null;
    let task_inst_id=req.body.task_inst_id ? req.body.task_inst_id:null
    let project_number=req.body.project_number ? req.body.project_number:null
    let vendor=req.body.vendor ? req.body.vendor:null;

    //req will request header
    let insertHeaderQuery = "insert into itential_cfg_header (csr_HostName,status,created_on)values(:csr_host,:status,TO_DATE(:created_on,'YYYY-MM-DD HH24:MI:SS'))"

    let insertHeaderParams = {
        csr_host: req.body.csr_router,
        status: 'New',
        created_on: timeStamp
    }
    let insertHeaderResult = await this.queryConfigDB(insertHeaderQuery, insertHeaderParams)
    console.log(insertHeaderResult)
    if (insertHeaderResult.resultcode == -1) {
        output.errors.push('Failed to get existing header')
        output.message = output.errors.join()
        return res.status(500).json(output)
    }
    let getHeaderQuery = 'select * from Itential_cfg_header where UUID=:UUID'
    let getHeaderParams = {
        UUID: UUID,
    }
    let getHeaderResult = await this.queryConfigDB(getHeaderQuery, getHeaderParams)
    if (getHeaderResult.resultcode == -1) {
        output.errors.push('Failed to insert header');
        return res.status(500).json(output)
    }
    if (getHeaderResult.result && getHeaderResult.result.length > 0 && getHeaderResult.result[0].Itential_cfg_header_id) {
        //Insert Details
        let insertDetailsQuery = "Insert into iteltial_cfg_details(initial_cfg_header_id,req_body,created_on) values(:req_header,:req_body,TO_DATE(:created_on,'YYYY-MM-DD HH24:MI:SS'))";
        let insertDetailsParams = {
            req_header: getHeaderResult.result[0].Itential_cfg_header_id,
            req_body: JSON.stringify(req.body),
            created_on: timeStamp
        }
        let insertDetailsResult = await this.queryConfigDB(insertDetailsQuery, insertDetailsParams)
        console.log(insertDetailsResult);
    } else {
        output.errors.push('No Request header found for UUID' + UUID)
    }
    if (output.errors.length > 0) {
        return res.status(500).json(output);
    }
    output.message = `Sucessfully inserted config in task for request header`
    output.Itential_cfg_header_id = getHeaderResult.result[0].Itential_cfg_header_id;
    console.log('executeItentialConfig -> \n req params :\n%j\n',req.body)
    close.log(getHeaderResult)
    let iopJobId='1';
    let description="test"
    let csrRouter="DLSDDSFDF-P-CI-0862-01"
    let currentCSRConfig="Current configuration : 22758 bytes \n!\n! Last configuration changes at 12:02:45 EST"
    if(getHeaderResult.result && getHeaderResult.result.length >0 && getHeaderResult.result[0].Itential_cfg_header_id){
        let headerReult=getHeaderResult.result[0];
        iopJobId=headerReult.Itential_cfg_header_id
        if(headerReult.csr_host){
            csrRouter=headerReult.csr_host
        }
        if(headerReult.description){
            description=headerReult.description
        }else{
            description="IOP -> itential for job"+iopJobId
        }
    }
    console.log('getting snapshot')
    let snapshot=await this.getRouterConfigFromRemote(csrRouter)
    snapshot=json.parse(snapshot)
    if(snapshot && snapshot.stdout && snapshot.stdout.length >0){
        currentCSRConfig=snapshot.stdout
        let login=''
        login=await this.loginNewRouter()
        //console.log(login)
        console.log("Current CSR")
        console.log(currentCSRConfig.length)
        let startWorkflowResult=await this.startWorkflowResult(iopJobId,csrRouter,currentCSRConfig,description,login)
    }else{
        output.errors.push({detail:'Could not find snapshot for'+csrRouter})
    }
    if(output.errors && output.errors.length >0){
        output.message='Failed to generate config'
        return res.status(500).json(output)
    }
    return res.status(200).json(output)
    }catch(err){
        logger.info("generate router config error occured",err);
        let errorMessage=err.message || err.detail || "Error occured while generating router config"
        let e= new error("500","Bad Request",errorMessage);
        output.errors.push(e);
        output.message=errorMessage;
        return res.status(500).json(output);
    }
}

module.exports.poupAction=async(req,res,next)=>{
    let output={
        message:'Success',
        errors:[]
    }
    try{
     let flag=req.body?.flag || '';
      if(!flag || flag ==''){
        let err= new error("500","Bad Request","Not a valid flag");
        resArray.push(err);
        output.errors=resArray;
        output.message="Fail to progress";
        return res.status(400).json(output);
      }
      let sendToItential=flag.toLocaleLowerCase() =='true' ? true:false;
      let iopJobId=req.body?.iopJobId || '';
      if(!iopJobId || iopJobId ===''){
        let err= new error("400","Bad Request","Not a valid iopJobId");
        resArray.push(err);
        output.errors=resArray;
        output.message="Fail to progress";
        return res.status(400).json(output);
      }
      let csrRouter=req.body?.csrRouter || '';
      if(!sendToItential ===true && (!csrRouter || csrRouter === '')){
        let err= new error("400","Bad Request","Not a valid csrRouter");
        resArray.push(err);
        output.errors=resArray;
        output.message="Fail to progress";
        return res.status(400).json(output);
      }
      let currentCSRConfig=req.body?.currentCSRConfig || '';
      if(!sendToItential ===true && (!currentCSRConfig || currentCSRConfig === '')){
        let err= new error("400","Bad Request","Not a valid currentCSRConfig");
        resArray.push(err);
        output.errors=resArray;
        output.message="Fail to progress";
        return res.status(400).json(output);
      }
      let description=req.body?.description || '';
      let result=await configWarning(sendToItential,iopJobId,csrRouter,currentCSRConfig,description)
      if(result && result.errors && result.errors.length >0){
        output.message=result?.message || '';
        output.errors=result.errors;
        return res.status(400).json(output);
      }
      return res.status(200).json(output);
    }catch(err){
        logger.info("generate router config error occured",err);
        let errorMessage=err.message || err.detail || "Error occured while generating router config"
        let e= new error("500","Bad Request",errorMessage);
        output.errors.push(e);
        output.message=errorMessage;
        return res.status(500).json(output);
    }
}
const configWarning=async(flag,iopJobId,csrRouter,currentCSRConfig,description)=>{
    let output={
        message:'',
        errors:[]
    }
    try{
        if(flag === null || flag === undefined){
            let err=new error("400","Bed request","Not a valid falg");
            output.errors.push(err);
            output.message="Not a valid falg";
            return output;
        }
        if(flag ===true){
            let login=''
            console.log("Current CSR");
            console.log(currentCSRConfig.length)
            let startWorkflowResult=await this.startWorkflow(iopJobId,csrRouter,currentCSRConfig,description,login)
            output.message="Generate router config Successfully";
        }else{
            let updateHeaderQuery="update itential_cfg_header set status=:status modified_on=To_Date(:modified_date,'YYYY-MM-DD HH24:MI:SS')where itential_cfg_header_id=:req_header)"
            let today=moment.utc()
            let timeStamp=moment(today).format(DATE_FORMAT)
            let updateHeaderParams={
                req_header:iopJobId,
                status:'Cancelled',
                modified_on:timeStamp
            }
            let updateHeaderResult=await this.queryConfigDB(updateHeaderQuery,updateHeaderParams)
            console.log(updateHeaderResult);
            output.message="update itential_cfg_header status Sucessfully"
         }
    }catch(error){
        logger.info("Processing generate router config error occured",err);
        let errorMessage=err.message || err.detail || "Error occured while Processing generate router config error occured"
        let e= new error("500","Bad Request",errorMessage);
        output.errors.push(e);
        output.message=errorMessage;
    }
    return output;
}