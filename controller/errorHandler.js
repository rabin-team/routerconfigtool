const globalError=require('../model/globalError');
const logger=require('../util/LogUtil');

module.exports = (err,res,req,next)=>{
    if((err instanceof globalError && err.status === 500) || err instanceof Error){
    logger.error(err.message);
    }
    if(err instanceof Error){
        err=[new globalError(500,'Internal Server Error',err.message)];
    }
    if(!Array.isArray(err)){
        err=[err];
    }
    res.status(err[0].status || 500).send({ errors: err});
}
