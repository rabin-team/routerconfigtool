let error = require('../model/globalError');
let restClient=require(rootdir + '/util/RestClient')
const client=require('../util/PromiseClient');
const fetch = require("node-fetch")
const moment = require('moment');
let _=require('lodash');
let RouterParamsHelpers=require(rootdir+'./model/RouterConfigModel');

const RouterConfigHelper=function(){

};
const addPorts=(obj,cb)=>{
    if(obj.portExist ==true)
    return cb();
    let url=config.dbservice.url+'/insert';
    let args={
        headers:{
            'Content-Type':'application/json',
        },
        data:{
            "data":{
               "queryParams":{
                "host_name":obj.host,
                "port_name":obj.port
               },
               "event":"insertNewPort"
            }
        }
    };
    restClient.post(url,args,function(data,response){
      if(data && response.statusCode==200){
        logger.info("Sucessfully inserted the new port as:"+obj.port+"for the host name"+obj.host);
      }else{
        logger.info("Failed inserted the new port as:"+obj.host);
        logger.info("response :: %j",data);
        let err="Error while inserting the hostname";
        cb(err);
      }
    });
}

module.exports=new RouterConfigHelper();