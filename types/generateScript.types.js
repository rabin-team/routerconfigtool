/**
    * @typedef {Object} ScriptInfo
    * @property {string} bandwidth
    * @property {string} bandwidth_type
    * @property {string} bdi300_ipv6
    * @property {string} bdi400_ipv6
    * @property {string} bvi_lte_ipv6
    * @property {string} enable_secret
    * @property {string} lookback300_ipv4
    * @property {string} lookback300_ipv6
    * * @property {EnodbInterface[]} enodebs
 */
/**
   * @typedef {Object} EnodbInterface
   * @property {string} Interface 
   * @property {string} id
 */

/**
   * @typedef {Object} ScriptResult
   * @property {string} status 
   * @property {string} status_message
   * @property {string} data
   * @property {string} data.zip
 */

/**
   * @typedef {Object} ConfigRequest
   * @property {string} config_params 
   * @property {string} config_params.csr_hostname
   * @property {string} config_params.csr_vendor
   * @property {string} config_params.csr_type
   * @property {string} config_params.created_by
 */