/**
This file is to mock all the globals before the tests run.
make sure this file is at the root level of the codebase
or else rootdir will point to the wrong directory causing errors.
 */
process.env.SUPPRESS_NO_CONFIG_WARNING =true;
process.env.NODE_ENV='test';

const config=require('config');

global.rootdir=__dirname;
global.config=config;
global.logger={
    info:jest.fn(),
    debug:jest.fn()
};
global.authUtil={
    isValidUser: jest.fn().mockReturnValue({code : 0})
}

global.userList={
    "Rabin":{
      "login_id":"thapra4",
      "fname":"Rabin",
      "lname":"Magar",
      "phone":"223-34-1234",
      "email":"rabin.thapa.magar@verizon.com",
      "status":"I",
      "role":"5G",
      "cellview":"no",
      "switchview":"no",
      "market":"South East"
    },
    "Sudha":{
        "login_id":"ranasu4",
        "fname":"Sudha",
        "lname":"Rana",
        "phone":"223-34-1234",
        "email":"sudha.rana.magar@verizon.com",
        "status":"I",
        "role":"5G",
        "cellview":"no",
        "switchview":"no",
        "market":"South East"
      }
}