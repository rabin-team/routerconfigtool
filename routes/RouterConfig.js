const router=require('express').Router();
const  Controller=require(rootdir + '/controller/RouterConfigController');
const  itentialController=require(rootdir + '/controller/ItentialController');
const error= require(rootdir + '/model/globalError')

/**
 * This API is used  to Fetch Roter config of Cisco and Ciena vendor
 */
 router.get('/router/config/values',(req,res,next) =>{
    Controller.saveRoutersPorts(req,res,next); 
})

router.get('/itential/views/request/:action/:task_inst_id',(req,res,next) =>{
    itentialController.getItentialRequestbyTask(req,res,next); 
})
router.post('/itential/generate/projectid',(req,res,next) =>{
    itentialController.getItentialconfigTask(req,res,next); 
})
router.post('/generate/config',(req,res,next) =>{
    Controller.generateRouterConfig(req,res,next); 
})
router.get('/itential/download/:request_id',(req,res,next)=>{
    itentialController.downloadItentialConfig(req,res,next);
});
router.get('/itential/delete/:request_id',(req,res,next)=>{
    itentialController.deleteItentialRequest(req,res,next);
});
router.get('/itential/help',(req,res,next)=>{
    itentialController.getHelpText(req,res,next);
});
router.get('/itential',(req,res,next)=>{
    return res.status(200).json({message:'OK'})
});
//Itential generate popup
router.post('/itential/checkhour',(req,res,next)=>{
    itentialController.checkHour(req,res,next);
});
router.post('/itential/popuoaction',(req,res,next)=>{
    itentialController.poupAction(req,res,next);
});
router.post('/itential/generateforciena',(req,res,next)=>{
    itentialController.generateforCiena(req,res,next);
});

module.exports =router;

