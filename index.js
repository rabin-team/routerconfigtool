//Initialize GLOBALS here
global.rootdir = __dirname; //ROOT level directory
global.config=require('config');
global.logger= require(rootdir + '/util/LogUtil');
global.globalErrorHandler= require(rootdir + '/controller/errorHandler');
global.globalError=require(rootdir + '/model/globalError');

//import modules
const express=require('express');
const app=express();
//call routes
const unitRoutes=require(rootdir + '/routes/unitRoutes');
const siteRoutes=require(rootdir+ './routes/siteRoutes');
const westellRoutes=require(rootdir+ '/routes/westellRoutes')
const swaggerUi=require('swagger-ui-express'),
    swaggerDocument=require('./swagger.json');
const morgan=require('morgan');
const cron=require('node-cron');
const { config } = require('dotenv');
require('dotenv').config();
app.use(express.urlencoded({ extended: true}));
app.use(express.json());

//not initilising morgan for Test env
if(process.env.NODE_ENV !== 'test'){
    app.use(morgan('dev'));
}

//add CORS Support
app.use(function(req,res,next){
    logger.info("Adding the CORS support inside the initilising function");
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers','Content-Type,Authorization');
    next();
})

//Trigger manual GC every 3 hours -Apps running unser PM2 issues
const runGC=function(){
 logger.info("Enter -run GC -- Time --> "+ new Date().toString());
 if(global.gc){
     global.gc();
     logger.info('After GC');
 }else{
     logger.info('Garbage collection unavailable. use --espose-gc'+ 'when launching node to enable forced garbage collection.');

 }
 logger.info("Exit -run GC --time -->"+ new Date().toString)

}
var intvar= setInterval(runGC,config.app.gcInterval);

//Routes
app.use('/routercfg/IPAM/v1',require('./routes/ServicePortal'));
var router=app.use('/routercfg',this.routes);
//Global error Handler
app.use(globalErrorHandler);
app.get('/',(req,res)=>{
  res.send({message: 'Welcome to the Router Config Tool'});
});
//start express server
const server=app.listen(config.app.port, ()=>{
  console.log(`Server running on port ${config.app.port}`);
});
module.exports=app;
