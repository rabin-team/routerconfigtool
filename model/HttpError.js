class HttpError extends Error{
    constructor(data, response,error,message="there was an unhandle http error"){
        super(error ? error.message:message);
        this.data=data;
        this.response=response;
        /** @deprecated here for backward compatibility */
        this.error=null;
    }
}
module.exports=HttpError;